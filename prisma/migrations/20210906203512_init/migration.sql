-- CreateTable
CREATE TABLE `example` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `textField` LONGTEXT NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
