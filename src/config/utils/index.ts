import DB from '../../dao';
import ErrorType from '../../lib/common/errors/ErrorTypes';
import Exception from '../../lib/common/errors/Exception';
export const db = () => {
    try{
        const choice = process.env.DATABASE_CHOICE || 'mysql'
        return DB[choice];
    } catch (e) {
        throw new Exception('Banco não especificado', ErrorType.InternalError);
    }
}