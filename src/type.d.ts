type Opaque<K, T> = T & { __TYPE__: K };
type Filepath=Opaque<'Path', String>;