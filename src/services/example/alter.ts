import exampleDBSchema from '../../interface/dbSchema';

export default async (database:exampleDBSchema, obj:any, id:number) => {
    return await database.exampleDB.Alter({...obj}, id);
}