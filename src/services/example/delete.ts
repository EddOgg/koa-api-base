import exampleDBSchema from '../../interface/dbSchema';

export default async (database:exampleDBSchema, id:string) => {
    return await database.exampleDB.Delete(id);
}