import Get from "./get"
import New from "./new"
import Alter from "./alter"
import Delete from "./delete"
export default {
    New,
    Get,
    Alter,
    Delete,
}