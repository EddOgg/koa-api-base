import exampleDBSchema from "../../interface/dbSchema";

export default async (database:exampleDBSchema, obj: any) => {
    return await database.exampleDB.Get({...obj });
}