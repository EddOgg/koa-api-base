import fs from 'fs';

async function asyncForEach(array:any[], callback:Function) {
  // eslint-disable-next-line no-plusplus
  for (let index = 0; index < array.length; index++) {
    // eslint-disable-next-line no-await-in-loop
    await callback(array[index]);
  }
}

export function walkSync(dir:string, filelist?:any[]) : Filepath[] {
  const files = fs.readdirSync(dir);
  let newFilelist = filelist || [];
  files.forEach(file => {
    if (fs.statSync(dir + file).isDirectory()) {
      newFilelist = walkSync(`${dir + file}/`, newFilelist);
    } else {
      newFilelist.push(`${dir + file}`);
    }
  });
  return newFilelist;
}

export default { asyncForEach, walkSync };
