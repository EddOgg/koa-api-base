require('dotenv').config()
import Koa from 'koa';
import koaBody from 'koa-body';
import Router from './controller';
import joiMiddleware from './lib/common/middlewares/joiMiddleware';
import swaggerMiddleware from './lib/common/middlewares/swaggerMiddleware';

const app = new Koa();
//TODO: Trocar os throw common errors por novo Exception
app.use(koaBody({
   multipart: true,
   formidable: {
     keepExtensions: true,
   },
 }));

 app.use(swaggerMiddleware);
 app.use(joiMiddleware);

app.use(Router.routes())
   .use(Router.allowedMethods());

app.listen(3000);