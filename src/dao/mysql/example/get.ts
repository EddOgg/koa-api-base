import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default async (example:any) => {
    try{
        const data = await prisma.example.findUnique({
            where: example
        });
        return { messagem: 'Exemplo encontrado', data };
    }catch (e) {
        throw new Error(`Deu problema, jovem! \n\r ${e.message}`);
    }
}