import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default async (alterExample:any, id: number) => {
    try{
        const data = await prisma.example.update({
            where: {
                id: id
            },
            data: alterExample
        });
        return { messagem: 'Exemplo atualizado!', data };
    }catch (e) {
        throw new Error(`Deu problema, jovem! \n\r ${e.message}`);
    }
}