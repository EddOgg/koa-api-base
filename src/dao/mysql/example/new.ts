import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default async (newExample:any) => {
    try{
        await prisma.example.create({
            data: newExample,
        });
        return { messagem: 'Sucesso!', data: {...newExample} };
    }catch (e) {
        throw new Error(`Deu problema, jovem! \n\r ${e.message}`);
    }
}