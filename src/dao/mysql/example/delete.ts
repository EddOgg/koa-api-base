import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default async (id: number) => {
    try{
        const data = await prisma.example.delete({
            where: {
                id: id
            }
        });
        return { messagem: 'Exemplo deletado!', data };
    }catch (e) {
        throw new Error(`Deu problema, jovem! \n\r ${e.message}`);
    }
}