import Alter from './alter'
import Delete from './delete'
import Get from './get'
import New from './new'

export default {
    Alter,
    Delete,
    Get,
    New,
}