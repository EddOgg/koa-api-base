import firebase from "./firebase";
import mysql from './mysql';

export default {
    firebase, mysql
}