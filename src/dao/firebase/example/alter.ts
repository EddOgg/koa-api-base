import db from '../../../config/firebase';

export default async (req: any) => {
    let data:any;
    let response = { messagem: "Examplo não cadastrado!", data };
    if (!req.id)
        throw new Error('Hash inválida');
    
    const user = await db.collection('examples').doc(req.id);
    const userData = await user.get();
    if (userData.exists){
        user.set(req.body, {merge: true})
        response = { messagem: 'Exemplo atualizado!', data};
    }
    return response;
}