import db from '../../../config/firebase';
import { getLogger } from '../../../lib/common/log';

export default async (body: any) => {
    const { error, info } = getLogger({});
    let response = { messagem: "Exemplo já cadastrado!", data: {} };
    const hash = body.hash || 'error';
    if (hash === 'error'){
        error('Hash inválida', body);
        throw new Error('Hash inválida');
    }

    try{
        const example = await db.collection('examples').where('hash', '==', hash).get();
        if (example.size === 0) {
            await db.collection('examples').doc(hash).set({...body, active: true});
            response = {
                messagem: '',
                data: {...body}
            }
        }
    } catch (e) {
        error('Error em salvar dados no banco', e);
        throw e;
    }
    return response;
}