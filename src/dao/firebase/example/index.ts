import NewUserDB from './new'
import GetUserDB from './get'
import AlterUserDB from './alter'
import DeleteUserDB from './delete'

export default {
    NewUserDB,
    GetUserDB,
    AlterUserDB,
    DeleteUserDB,
}