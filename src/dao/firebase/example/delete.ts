import db from '../../../config/firebase';

export default async (id: string) => {
    let data:any;
    let response = { messagem: "Exemplo não cadastrado!", data };
    if (!id)
        throw new Error('Hash inválida');
    
    const example = await db.collection('examples').doc(id);
    const userData = await example.get();
    if (userData.exists){
        example.set({ active: false }, {merge: true})
        response = { messagem: 'Exemplo desativado com sucesso!', data};
    }
    return response;
}