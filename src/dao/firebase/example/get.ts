import db from '../../../config/firebase';

export default async (body: any) => {
    let data:any;
    if (body.hash){
        const example = await db.collection('examples').doc(body.hash).get();
        if (!example.exists) {
            throw new Error("404");
        }
        const data:any = example.data();
        if(!data.active){
            throw new Error('403');
        }
        return { messagem: 'Usuário encontrado', data: {...data, password: undefined} }
    }
}