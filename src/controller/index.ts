import { KJSRouter } from 'koa-joi-swagger-ts';
import { walkSync } from '../lib/utils/walkSync';

const router = new KJSRouter({
    swagger: '2.0',
    info: {
      version: '1.0.0',
      title: 'CRUD Example',
    },
    basePath: '',
    schemes: ['http'],
    paths: {},
    definitions: {},
  });

// Controllers Dynamic Auto Loader
// Para carregar automaticamente um controller basta nomear seu controller como
// `controller.ts` dentro de alguma pasta nesse diretorio
const controllerPaths = walkSync(`${__dirname}/`)
  .filter((filepath:Filepath) => filepath.includes('/controller.js') || filepath.includes('/controller.ts'))
  .filter((filepath:Filepath) => !filepath.includes('/controller.js.map'))
  // Essa linha ordena os controllers mais especificos para serem importados primeiro
  // sem ela um path menos especifico pode sobreescrever o mais especifico.
  .sort((a, b) => b.length - a.length);

controllerPaths.forEach((filepath:Filepath) => {
  router.loadController(require(`${filepath}`).default);
});

// Swagger
// TODO: Descobrir por que rotas de tipo diferente mas mesma nomeclatura de rota não aparecem corretamente
router.setSwaggerFile('swagger.json');
router.loadSwaggerUI('/swagger');

export default router.getRouter();
