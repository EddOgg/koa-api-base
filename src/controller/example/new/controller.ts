import Joi from 'joi';
import { controller, ENUM_PARAM_IN, parameter, post, response } from 'koa-joi-swagger-ts';
import { db } from '../../../config/utils';
import { getLogger } from '../../../lib/common/log';
import services from '../../../services';
import Exception from '../../../lib/common/errors/Exception';
import ErrorType from '../../../lib/common/errors/ErrorTypes';

const exampleJoi = Joi.object({
    textField: Joi.string().required().example('Teste'),
});

@controller('/example')
export default class NewExample {
    @post('/n')
    @parameter('example ',
        exampleJoi,
        ENUM_PARAM_IN.body)
    @response(201, Joi.object().description('Exemplo cadastrado com sucesso.'))
    async newExample(ctx): Promise<void> {
        const { error } = getLogger(ctx);
        try {
            const response = await services.Example.New(db(), ctx.request.body);
            ctx.body = response;
        } catch (e) {
            throw new Exception('Erro ao cadastrar o exemplo!', ErrorType.NotFound, e);
        }
    }
}