import Joi from 'joi';
import { controller, ENUM_PARAM_IN, parameter, get } from 'koa-joi-swagger-ts';
import services from '../../../services';
import { db } from '../../../config/utils';
import JWT from '../../../lib/common/auth/permissionDecorator';
import Exception from '../../../lib/common/errors/Exception';
import ErrorType from '../../../lib/common/errors/ErrorTypes';

@controller('/example')
export default class GetExample {
    @get('/g/{id}')
    @parameter("id", Joi.string().description('Id do Exemplo').required(), ENUM_PARAM_IN.path)
    @JWT()
    async getExample(ctx): Promise<void> {
        try {
            const { id } = ctx.params
            const response = await services.Example.Get(db(), {id});
            ctx.body = response;
        } catch (e) {
            switch (e.message) {
                case '403':
                    throw new Exception('Examplo inativado!', ErrorType.Forbidden, e);
                    break;
                case '404':
                    throw new Exception('Exemplo não encontrado!', ErrorType.NotFound, e);
                    break;
            
                default:
                    throw new Exception('Ops... Um erro não esperado ocorreu!', ErrorType.InternalError, e);
                    break;
            }
        }
    }
}