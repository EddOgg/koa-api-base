import Joi from 'joi';
import { controller, ENUM_PARAM_IN, parameter, del } from 'koa-joi-swagger-ts';
import { db } from '../../../config/utils';
import services from '../../../services';
import JWT from '../../../lib/common/auth/permissionDecorator';
import Exception from '../../../lib/common/errors/Exception';
import ErrorType from '../../../lib/common/errors/ErrorTypes';

@controller('/example')
export default class DeleteExample {
    @del('/d/{id}')
    @parameter("id", Joi.string().description('ID do exemplo').required(), ENUM_PARAM_IN.path)
    @JWT()
    async deleteExample(ctx): Promise<void> {
        try {
            const { id } = ctx.validatedParams
            const response = await services.Example.Delete(db(), id);
            ctx.body = response;
        } catch (e) {
            throw new Exception('Erro na deleção do exemplo!', ErrorType.InternalError, e);
        }
    }
}