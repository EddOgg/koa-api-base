import { db } from '../../../config/utils';
import services from '../../../services';
import JWT from '../../../lib/common/auth/permissionDecorator';
import Joi from 'joi';
import { controller, ENUM_PARAM_IN, parameter, patch } from 'koa-joi-swagger-ts';
import Exception from '../../../lib/common/errors/Exception';
import ErrorType from '../../../lib/common/errors/ErrorTypes';
const exampleJoi = Joi.object({
    "firstName": Joi.string().required(),
    "password": Joi.string().required(),
})

@controller('/example')
export default class AlterExample {
    @patch('/a/{id}')
    @parameter("id", Joi.string().description('ID do exemplo').required(), ENUM_PARAM_IN.path)
    @parameter('example', exampleJoi, ENUM_PARAM_IN.body)
    @JWT()
    async alterExample(ctx): Promise<void> {
        try {
            const { id } = ctx.params
            const response = await services.Example.Alter(db(), { body: ctx.request.body }, id);
            ctx.body = response;
        } catch (e) {
            throw new Exception('Falha na alteração de exemplo', ErrorType.InternalError, e);
        }
    }
}