export default interface exampleDBSchema {
    exampleDB: {
        Alter: any;
        Delete: any;
        Get: any;
        New: any;
    }
}