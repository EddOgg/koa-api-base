# Base em KOA com Swagger/Joi

Framework+Boilerplates para criação de microsserviços, tanto utilizando MYSQL quanto Firestore (Firebase - Google)


## *Documentações necessárias para seguir com esta versão:*

[Prisma(Client)](https://www.prisma.io/docs/)
[Koa-Joi-Swagger-TS](https://github.com/DragFAQ/koa-joi-swagger-ts)
[Firebase: Firestore](https://firebase.google.com/docs/firestore)


**Exemplo de Env:**

> #Tipo de Banco usado, sendo mysql ou firebase
> DATABASE_CHOICE=mysql
> 
> #Informações do Firebase, caso o use
> FIREBASE_API_KEY=<<api_key>>
> FIREBASE_AUTH_DOMAIN=<<auth_domin>>
> FIREBASE_DB_URL=<<fb_url>>
> FIREBASE_PROJECT_ID=<<project_id>>
> FIREBASE_STORAGE_BUCKET=<<storage_bucket>>
> FIREBASE_MESSAGING_SENDER_ID=<<messaging_sender_id>>
> FIREBASE_APP_ID=<<firebase_app_id>>
> PROJECT_KEY="*/../project-key.json"
> 
> #String de conexão do banco MYSQL
> DATABASE_URL="mysql://root:admin@localhost:3306/application"
> 
> #BaseURL e Secret para JWT
> BASEURL="localhost:3000"
> JWT_SECRET="12345"


## *Executando pela primeira vez*

 - [ ] Gere o .env do projeto
 - [ ] Caso seja Firebase, certifique de estar com o certificado corretamente
 - [ ] Caso seja MYSQL, certifique de executar o migrate do Prisma
 - [ ] Esteja confiante e build sua aplicação.


***Pontos interessantes:***

 - Prisma aceita tanto fazer migrations, quando importar para si os
   dados do banco, usando migrate ou generate. Tente manter um padrão
   usando somente um deles. 
 - Koa-Joi-Swagger-TS não tem atualização a algum tempo, caso interesse, puxem um fork dele ou sinta-se livre para utilizar outro.
 - Há um submodule no projeto!